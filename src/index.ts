import log4js from 'log4js';
import { IndexController } from './controller';
import { UserController } from './controller/user-controller';
import dotenv from 'dotenv';
import express, { Express, RequestHandler } from 'express';
import bodyParser from 'body-parser';
import httpContext from 'express-http-context';
import { useExpressServer } from 'routing-controllers';
import { GlobalErrorHandler } from './middleware/global-error-handler';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from '../src/swagger/openapi.json';
import cors from 'cors';

dotenv.config();

const logger = log4js.getLogger();
logger.level = process.env.LOG_LEVEL;

var whitelist = ['http://example1.com', 'http://example2.com']
// eslint-disable-next-line no-unused-vars
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}


const app: Express = express();
app.use(cors() as RequestHandler);
app.use(bodyParser.json());
app.use(httpContext.middleware);

useExpressServer(app, {
  controllers: [IndexController, UserController],
  middlewares: [GlobalErrorHandler],
  defaultErrorHandler: false
});
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use((req, res, next) => {
  httpContext.ns.bindEmitter(req);
  httpContext.ns.bindEmitter(res);
  next();
});

const port = process.env.PORT;

app.listen(port, () => console.log(`Running on port ${port}`));
