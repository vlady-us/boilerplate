import { Controller, Get, UseBefore, UseAfter, UseInterceptor, Action } from 'routing-controllers';
import 'reflect-metadata';
import { loggingAfter, loggingBefore } from '../middleware/middleware';

@Controller()
@UseBefore(loggingBefore)
@UseAfter(loggingAfter)
@UseInterceptor(function (action: Action, context:any) {
  console.log('change context');
  context = 'interceptor';
  return context;
})
export class IndexController {
  @Get('/')
  getOne () {
    return 'Main page';
  }
}
