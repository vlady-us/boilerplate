import httpContext from 'express-http-context';

export function loggingBefore (request: any, response: any, next?: (err?: any) => any): any {
  console.log('do something Before...');
  httpContext.set('traceId', 734654563563);
  next();
}

export function loggingAfter (request: any, response: any, next?: (err?: any) => any): any {
  console.log(`traceId ${httpContext.get('traceId')}`);
  next();
}
